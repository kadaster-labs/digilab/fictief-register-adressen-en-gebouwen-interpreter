package model

import "github.com/google/uuid"

type AddressRegisteredEvent struct {
	Street              string       `json:"street"`
	HouseNumber         int32        `json:"houseNumber"`
	HouseNumberAddition string       `json:"houseNumberAddition,omitempty"`
	ZipCode             string       `json:"zipCode"`
	Municipality        Municipality `json:"municipality"`
	Purpose             string       `json:"purpose"`
	Surface             int32        `json:"surface"`
}

type AddressEvent struct {
	ID                  uuid.UUID `json:"id"`
	Street              string    `json:"street"`
	HouseNumber         int32     `json:"houseNumber"`
	HouseNumberAddition string    `json:"houseNumberAddition,omitempty"`
	ZipCode             string    `json:"zipCode"`
	Municipality        string    `json:"municipality"`
	MunicipalityCode    string    `json:"municipalityCode"`
	Purpose             string    `json:"purpose"`
	Surface             int32     `json:"surface"`
}
