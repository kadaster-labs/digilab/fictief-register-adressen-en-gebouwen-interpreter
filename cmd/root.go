package cmd

import (
	"github.com/spf13/cobra"
)

var RootCmd = &cobra.Command{ //nolint:exhaustruct,gochecknoglobals // this is the recommended way to use cobra
	Use:   "api",
	Short: "frv interpreter",
	Long:  "Fictief register adressen en gebouwen interpreter",
}

func Execute() error {
	return RootCmd.Execute() //nolint:wrapcheck // not necessary
}

func init() { //nolint:gochecknoinits // this is the recommended way to use cobra
	RootCmd.AddCommand(interpreterCommand)
}
