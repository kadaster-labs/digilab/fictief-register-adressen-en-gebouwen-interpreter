package application

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-interpreter/model"
)

func (app *Application) HandleAddressRegistered(ctx context.Context, event *model.Event) error {
	data := new(model.AddressRegisteredEvent)
	if err := json.Unmarshal(event.EventData, data); err != nil {
		return fmt.Errorf("json unmarshal failed: %w", err)
	}

	if app.Filter(data.Municipality.Code) {
		return nil
	}

	address := model.AddressEvent{
		ID:                  event.SubjectIDs[0],
		Street:              data.Street,
		HouseNumber:         data.HouseNumber,
		HouseNumberAddition: data.HouseNumberAddition,
		ZipCode:             data.ZipCode,
		Municipality:        data.Municipality.Name,
		MunicipalityCode:    data.Municipality.Code,
		Purpose:             data.Purpose,
		Surface:             data.Surface,
	}

	if err := app.Request(ctx, http.MethodPost, "/addresses", address, nil); err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	return nil
}

func (app *Application) HandleAddressRemoved(ctx context.Context, event *model.Event) error {
	if err := app.Request(ctx, http.MethodDelete, fmt.Sprintf("/addresses/%s", event.SubjectIDs[0]), nil, nil); err != nil {
		return fmt.Errorf("request failed; %w", err)
	}

	return nil
}
