package application

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"

	"github.com/nats-io/nats.go"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-interpreter/model"
)

type Application struct {
	client       *http.Client
	municipality string
	baseURL      string
}

var ErrResponseFailed = errors.New("response failed with")

func NewApplication(municipality, baseURL string) *Application {
	return &Application{
		client:       http.DefaultClient,
		municipality: municipality,
		baseURL:      baseURL,
	}
}

func (app *Application) HandleEvents(msg *nats.Msg) {
	event := new(model.Event)
	if err := json.Unmarshal(msg.Data, event); err != nil {
		log.Println(err)
	}

	switch event.EventType {
	case "AdresGeregistreerd":
		if err := app.HandleAddressRegistered(context.Background(), event); err != nil {
			fmt.Print(fmt.Errorf("handle address registered event failed: %w", err))
		}
	case "AdresToewijzing":
		if err := app.HandleBuildingAttachAddress(context.Background(), event); err != nil {
			fmt.Print(fmt.Errorf("handle buildings attach address event failed: %w", err))
		}
	case "GebouwGeregistreerd":
		if err := app.HandleBuildingRegistered(context.Background(), event); err != nil {
			fmt.Print(fmt.Errorf("handle building registered event failed: %w", err))
		}
	case "SloopAfgerond":
		if err := app.HandleBuildingDemolished(context.Background(), event); err != nil {
			fmt.Print(fmt.Errorf("handle building demolished event failed: %w", err))
		}
	case "AdresIngetrokken":
		if err := app.HandleAddressRemoved(context.Background(), event); err != nil {
			fmt.Print(fmt.Errorf("handle address removed event failed: %w", err))
		}
	default:
		fmt.Printf("event type: %s unknown\n", event.EventType)
	}
}

// Filter
// Filter will check if the given municipality equals the configuration based municipality
// If the configured municipality is empty we dont filter at all
//
// Return val true -> remove value;
// Return val false -> keep value.
func (app *Application) Filter(municipality string) bool {
	if app.municipality == "" {
		return false
	}

	if app.municipality == municipality {
		return false
	}

	return true
}

func (app *Application) Request(ctx context.Context, method, path string, data, value any) error {
	buf := new(bytes.Buffer)
	if data != nil {
		if err := json.NewEncoder(buf).Encode(data); err != nil {
			return fmt.Errorf("encoding failed: %w", err)
		}
	}

	req, err := http.NewRequestWithContext(ctx, method, app.baseURL+path, buf)
	if err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "*/*")
	req.Header.Set("Cache-Control", "no-cache")

	resp, err := app.client.Do(req)
	if err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	defer resp.Body.Close()

	if resp.StatusCode >= http.StatusBadRequest {
		body, _ := httputil.DumpResponse(resp, true)

		return fmt.Errorf("%w: %s", ErrResponseFailed, string(body))
	}

	if value != nil {
		if err := json.NewDecoder(resp.Body).Decode(value); err != nil {
			return fmt.Errorf("decoding failed: %w", err)
		}
	}

	return nil
}
