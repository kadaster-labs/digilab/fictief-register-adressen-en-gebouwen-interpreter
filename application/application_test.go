package application_test

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-interpreter/application"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-interpreter/model"
)

func TestFilter(t *testing.T) {
	type testcase struct {
		name         string
		municipality string
		expCall      bool
	}

	for _, tc := range []testcase{
		{
			name:         "no filter",
			municipality: "",
			expCall:      true,
		},
		{
			name:         "same municipality",
			municipality: "999",
			expCall:      true,
		},
		{
			name:         "different municipality",
			municipality: "998",
			expCall:      false,
		},
	} {
		t.Run(tc.name, func(t *testing.T) {
			didCall := false

			server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				switch r.URL.String() {
				case "/addresses":
					didCall = true
				}
				fmt.Printf("r.URL.String(): %v\n", r.URL.String())
			}))

			defer server.Close()

			app := application.NewApplication(tc.municipality, server.URL)

			eventData := model.AddressRegisteredEvent{
				Municipality: model.Municipality{
					Name: "foo",
					Code: "0999",
				},
			}

			data, err := json.Marshal(eventData)
			assert.NoError(t, err)

			event := &model.Event{
				ID: uuid.New(),
				SubjectIDs: []uuid.UUID{
					uuid.New(),
				},
				EventType: "AdresGeregistreerd",
				EventData: data,
			}

			if err := app.HandleAddressRegistered(context.Background(), event); err != nil {
				assert.NoError(t, err)
			}

			assert.Equal(t, tc.expCall, didCall)
		})
	}
}
