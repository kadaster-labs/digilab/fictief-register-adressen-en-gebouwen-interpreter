package application

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-adressen-en-gebouwen-interpreter/model"
)

func (app *Application) HandleBuildingRegistered(ctx context.Context, event *model.Event) error {
	data := new(model.BuildingRegisteredEvent)
	if err := json.Unmarshal(event.EventData, data); err != nil {
		return fmt.Errorf("json unmarshal failed: %w", err)
	}

	if app.Filter(data.Municipality.Code) {
		return nil
	}

	building := model.BuildingEvent{
		ID:            event.SubjectIDs[0],
		ConstructedAt: data.ConstructedAt,
		Surface:       data.Surface,
	}

	if err := app.Request(ctx, http.MethodPost, "/buildings", building, nil); err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	return nil
}

func (app *Application) HandleBuildingAttachAddress(ctx context.Context, event *model.Event) error {
	data := new(model.AttachBuildingsToAddressEvent)
	if err := json.Unmarshal(event.EventData, data); err != nil {
		return fmt.Errorf("json unmarshal failed: %w", err)
	}

	if app.Filter(data.Municipality.Code) {
		return nil
	}

	item := model.AttachBuildingsToAddress{
		Buildings: []uuid.UUID{event.SubjectIDs[0]},
		Addresses: []uuid.UUID{data.Address},
	}

	if err := app.Request(ctx, http.MethodPost, "/buildings/attach/address", item, nil); err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	return nil
}

func (app *Application) HandleBuildingDemolished(ctx context.Context, event *model.Event) error {
	if err := app.Request(ctx, http.MethodDelete, fmt.Sprintf("/buildings/%s", event.SubjectIDs[0]), nil, nil); err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	return nil
}
